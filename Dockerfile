FROM ubuntu:16.04

LABEL ch.adiutec.vendor="Adiutec AG"

LABEL description="Adiutec AG Ubuntu Linux Docker image with several tools for teaching networking"

RUN apt-get update && apt-get install -y \
    net-tools \
    iputils-ping \
    inetutils-traceroute \
    lynx-cur \
    ftp \
    dnsutils \
    telnet \
    ssh \
    man \
    && rm -rf /var/lib/apt/lists/*

COPY interfaces /etc/network/interfaces

ENV HOME /root

WORKDIR /root

CMD ["bash"]
