# Networking Ubuntu Docker Container

This container provides several tools:

* ifconfig (from package net-tools)
* ping (from package iputils-ping)
* arp
* traceroute (from package inetutils-traceroute)
* lynx webbrowser (from package lynx-cur)
* ftp (from package ftp)
* nslookup (from package dnsutils)
* host (from package dnsutils)
* dig (from package dnsutils)
* telnet (from package telnet)
* ssh (from package ssh)
* man (from package man)

It's designed to be used in GNS3 as a docker container

## How to use

For running directly with docker, just start it directly from the gitlab
registry of this project. Example:
`docker run -it --rm registry.gitlab.com/aag-net-class/aag-net-class-docker-ubuntu:latest`

For GNS3, just import the appliance provided by 
`aag-net-class-docker-ubuntu.gns3a`. Alternatively, add it manually with
`registry.gitlab.com/aag-net-class/aag-net-class-docker-ubuntu:latest` as
`Image name` in Dialog under `Preferences`/`Docker Containers`/`New`.

## How to build and publish

1. Execute Docker builder, e.g.
   `docker build -t registry.gitlab.com/aag-net-class/aag-net-class-docker-ubuntu:20180121-0033 -t registry.gitlab.com/aag-net-class/aag-net-class-docker-ubuntu:latest .`
2. After testing, upload the image to the registry with
   `docker push registry.gitlab.com/aag-net-class/aag-net-class-docker-ubuntu`

## Limitations and corresponding remarks

`w3m` isn't working in the GNS3 console under windows. To have a
webbrowser, `lynx` is available.
